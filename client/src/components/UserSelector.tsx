import { Author } from "../entities"

interface UserSelectorProps {
  selfId: number,
  setSelfId: (selfId: number) => any,
  authors: Author[]
}

export const UserSelector = ({selfId, setSelfId, authors}: UserSelectorProps) => {
  return <div id="userSelector">
    <div>Choose who you are:</div>
    <select value={selfId} onChange={(event) => setSelfId(parseInt(event.target.value))}>
      {authors.map((author) => <option key={author.id} value={author.id}>{author.name}</option>)}
    </select>
  </div>
}