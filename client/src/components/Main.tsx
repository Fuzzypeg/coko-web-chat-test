import { gql, useQuery } from "@apollo/client";
import React, { useState } from "react";
import { Author } from "../entities";
import { PostEditor } from "./PostEditor";
import { PostsList } from "./PostsList";
import { UserSelector } from "./UserSelector";

interface AuthorsData {
  authors: Author[];
}
const GET_AUTHORS = gql`
    query GetAuthors {
      authors {
        id,
        name
      }
    }
`;

export const Main = () => {
  const [selfId, setSelfId] = useState(1);

  let authors: Author[];
  {
    const { loading, error, data } = useQuery<AuthorsData>(GET_AUTHORS, { variables: { } });
    authors = data?.authors ?? [];
  }

  return (
    <div>
      <UserSelector selfId={selfId} setSelfId={setSelfId} authors={authors} />
      <div id="centralColumn">
        <PostsList selfId={selfId} authors={authors} />
        <PostEditor selfId={selfId} authors={authors} />
      </div>
    </div>
  );
}
