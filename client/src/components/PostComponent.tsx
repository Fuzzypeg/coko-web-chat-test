import React, { MouseEvent } from "react";
import { Author } from "../entities";
import { findMention } from "../mentions";

interface PostProps {
  authorName: string, time: string, wasUpdated: boolean, message: string, id: number, isSelf: boolean, authors: Author[]
}

interface PostState {
  isShowingContextMenu: boolean,
  menuPos: {x: number, y: number}
}


export class PostComponent extends React.Component<PostProps, PostState> {
  constructor(props: PostProps) {
    super(props);
    this.state = {isShowingContextMenu: false, menuPos: {x: 0, y: 0}};
    this.toggleContextMenu = this.toggleContextMenu.bind(this);
    this.editPost = this.editPost.bind(this);
    this.deletePost = this.deletePost.bind(this);
  }

  render() {
    return <div>
      {this.props.authorName && <div className="author">{this.props.authorName}</div>}
      {this.props.time && <div className="time">{this.props.time}</div>}
      {this.props.wasUpdated && <div className="update">{this.props.message.length > 0 ? "Updated" : "Deleted"}</div>}
      {this.props.message.length > 0 && <div className="message" onContextMenu={this.toggleContextMenu}>
        {this.state.isShowingContextMenu && <div className="menuShield" onClick={this.toggleContextMenu}>
          <PostContextMenu onEditClicked={this.editPost} onDeleteClicked={this.deletePost}/>
        </div>}
        {this.renderMessageFragments(this.props.message)}
      </div>}
  </div>;
  }

  renderMessageFragments(message: string): JSX.Element[]
  {
    const components = [];
    let searchStart = 0;
    let findResult: {start: number, end: number} | undefined;
    while((findResult = findMention(message, searchStart, this.props.authors))) {
      if (findResult.start > searchStart) {
        const clearText = message.substring(searchStart, findResult.start);
        components.push(<span key={components.length}>{clearText}</span>);
      }
      components.push(<strong key={components.length}>{message.substring(findResult.start, findResult.end)}</strong>);
      searchStart = findResult.end;
    }
    if (searchStart < message.length)
      components.push(<span key={components.length}>{message.substring(searchStart)}</span>);
    return components;
  }

  toggleContextMenu(event: MouseEvent<HTMLDivElement, any>): void {
    event.preventDefault();
    this.setState((state) => {return {isShowingContextMenu: this.props.isSelf && !state.isShowingContextMenu}});
  }

  editPost() {
    window.alert("TODO: allow editing of post " + this.props.id);
  }

  deletePost() {
    window.alert("TODO: delete post " + this.props.id);
  }
}

const PostContextMenu = (props: {onEditClicked: () => any, onDeleteClicked: () => any}) => {
  return <div className="contextMenu">
    <div onClick={props.onEditClicked}>Edit</div>
    <div onClick={() => {if (window.confirm("Delete this post?")) props.onDeleteClicked()}}>Delete</div>
  </div>;
}
