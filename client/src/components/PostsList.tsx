import { gql, useQuery } from '@apollo/client';
import React from 'react';
import { Author, Post } from '../entities';
import { NewPostNotification } from './NewPostNotification';
import { PostComponent } from './PostComponent';

interface PostsListProps {
  selfId: number,
  authors: Author[]
}

interface PostsData {
  posts: Post[];
}

const GET_POSTS = gql`
    query GetPosts {
      posts {
        id,
        author {
          id,
          #name  Only intermittently works to retrieve name from author! It's a server-side problem, as graphiql experiences the same issue.
        },
        message,
        createdAt
      }
    }
`;


export const PostsList = ({ selfId, authors }: PostsListProps) => {
  const { loading, error, data } = useQuery<PostsData>(GET_POSTS, { variables: { } });
  
  const simplifiedPosts = getSimplifiedPosts(data?.posts ?? [], authors);

  return (
    <div className="postsList">
      {loading && <div>Loading...</div>}
      {error && <div>Error! {error.message}</div>}
      {simplifiedPosts.map(post => (<div className={"post" + (post.author.id === selfId ? " self" : "")} key={post.id}>
        {post.dateString && <PostDate date={post.dateString}/>}
        <PostComponent authorName={post.author.name} time={post.timeString} wasUpdated={post.wasUpdated} message={post.message} id={post.id} isSelf={post.author.id === selfId} authors={authors}/>
      </div>))}
      <NewPostNotification selfId={selfId} authors={authors}/>{/*If I can get this working, try instead using a nested container with subscribeToNewComments={() => subscribeToMore( .... )} See https://www.apollographql.com/docs/react/data/subscriptions/*/}
    </div>
  );
}

const PostDate = (props: {date: string}) => {
  return <div className="date">{props.date}</div>;
}

// Drop author name for repeated posts; generate date field whenever a new date ticks over and time fields when minute ticks over
function getSimplifiedPosts(posts: Post[], authors: Author[]): Post[]
{
  const simplifiedPosts: Post[] = [];
  let lastAuthorId = 0;
  let lastPostDate = "";
  let lastPostTime = "";
  for (let post of posts) {
    const simplifiedPost: Post = { ...post };
    simplifiedPost.author = { ...post.author };
    simplifiedPost.author.name = authors.find((author) => {return author.id === post.author.id})?.name ?? "?!!"; //TODO Hack to get author name because the GET_POSTS query can't do it

    const createdDateForBrowser = new Date(post.createdAt * 1); //Converts the date into the client's timezone //TODO Why does it fail to create a valid date without this * 1 hack?
    const postDate = createdDateForBrowser.getFullYear() + "-" + pad(createdDateForBrowser.getMonth() + 1) + "-" + pad(createdDateForBrowser.getDate());
    const postTime = pad(createdDateForBrowser.getHours()) + ":" + pad(createdDateForBrowser.getMinutes());

    const dateChanged = postDate !== lastPostDate;
    const timeChanged = postTime !== lastPostTime || dateChanged;
    const authorChanged = post.author.id !== lastAuthorId;

    simplifiedPost.dateString = dateChanged ? postDate : "";
    simplifiedPost.timeString = (timeChanged || authorChanged) ? postTime : "";
    simplifiedPost.author.name = (authorChanged || dateChanged) ? simplifiedPost.author.name : "";

    simplifiedPosts.push(simplifiedPost);
    lastAuthorId = post.author.id;
    lastPostDate = postDate;
    lastPostTime = postTime;
  }
  return simplifiedPosts;
}

function pad(value: number)
{
  let result = String(value);
  return result.length < 2 ? "0" + result : result;
}
