import { useSubscription, gql } from '@apollo/client';
import { Author } from '../entities';
import { PostComponent } from './PostComponent';

interface NewPostNotificationProps {
  selfId: number,
  authors: Author[]
}

const POST_ADDED_SUBSCRIPTION = gql`
    subscription onPostAdded {
      postAdded {
        id,
        author {
          id,
          name
        },
        message,
        createdAt
      }
    }
`;

export const NewPostNotification = ({selfId, authors}: NewPostNotificationProps) => {
  const { data, loading } = useSubscription(POST_ADDED_SUBSCRIPTION, { variables: { } });
  
  if (loading)
    return <div>Post subscriber loading...</div>;
  
  return (
    <PostComponent
      authorName={data.postAdded.author.name}
      time={String(data.postAdded.createdAt)}
      wasUpdated={data.postAdded.wasUpdated}
      message={data.postAdded.message}
      id={data.postAdded.id}
      isSelf={data.postAdded.author.id === selfId}
      authors={authors}/>
  );
}