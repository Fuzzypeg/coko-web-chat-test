import { gql, useMutation } from '@apollo/client';
import React, { useState } from "react";
import { Author, Post } from "../entities";
import { findAllMentions } from '../mentions';

interface PostEditorProps {
  selfId: number,
  authors: Author[]
}

interface AddPostData {
  authorId: number,
  message: string,
  mentionedAuthorIds?: number[]
}

const ADD_POST = gql`
    mutation AddPost($authorId: Int!, $message: String!, $mentionedAuthorIds: [Int!]) {
      addPost(authorId: $authorId, message: $message, mentionedAuthorIds: $mentionedAuthorIds) {
        id
      }
    }
`;

export const PostEditor = ({selfId, authors}: PostEditorProps) => {
  const [message, setMessage] = useState("");
  const [mentions, setMentions] = useState([0]); //Dummy value is simpler than annotating type
  const [addPost, {error, data}] = useMutation<Post, AddPostData>(ADD_POST);

  if (error)
    return <div>Error! {error.message}</div>;

  return <div id="postEditor">
    <textarea className="editArea" value={message} onChange={e => setMessage(e.target.value)} onKeyPress={handleKeyPress} />
    <button className="submitButton" onClick={() => postMessage()}>Send</button>
  </div>;

  function handleKeyPress(event: React.KeyboardEvent<HTMLTextAreaElement>) {
    if (message.length > 0 && event.key === "Enter") {
      postMessage();
    }
  }

  function postMessage() {
    setMentions(findAllMentions(message, authors));
    addPost({ variables: { message: message, authorId: selfId, mentionedAuthorIds: mentions } });
    setMessage("");
  }
}
