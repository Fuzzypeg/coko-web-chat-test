export interface Author {
  id: number,
  name: string,
  isActive: boolean
}

export interface Post {
  id: number,
  author: Author,
  message: string,
  createdAt: number, //Unix timestamp in milliseconds
  updatedAt: number, //Unix timestamp in milliseconds
  wasUpdated: boolean,
  dateString: string, //Convenience property for use in component, not received from graphql
  timeString: string //Convenience property for use in component, not received from graphql
}
