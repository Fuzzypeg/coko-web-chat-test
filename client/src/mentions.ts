import { Author } from "./entities";

export const findMention = (message: string, searchStart: number, authors: Author[]): {start: number, end: number, authorId: number} | undefined => {
  let mentionTagPos = message.indexOf("@", searchStart);
  while (mentionTagPos >= 0) {
    const afterMentionTag = message.substring(mentionTagPos + 1);
    const matchedAuthor = authors.find((author) => afterMentionTag.startsWith(author.name));
    if (matchedAuthor)
      return {start: mentionTagPos, end: mentionTagPos + 1 + matchedAuthor.name.length, authorId: matchedAuthor.id};
    mentionTagPos = message.indexOf("@", mentionTagPos + 1);
  }
  return undefined;
}

export const findAllMentions = (message: string, authors: Author[]): number[] => {
  const result = [];
  let searchStart = 0;
  while (true) {
    let mention: {start: number, end: number, authorId: number} | undefined;
    if ((mention = findMention(message, searchStart, authors))) {
      result.push(mention.authorId);
      searchStart = mention.end;
    }
    else {
      return result;
    }
  }
}
