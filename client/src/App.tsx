import apolloClient from './apolloSetup';
import { ApolloProvider } from '@apollo/client';
import { Main } from './components/Main';

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <Main />
    </ApolloProvider>
  );
}

export default App;
