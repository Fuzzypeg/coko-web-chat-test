import { Entity, ManyToOne, PrimaryKey, Property } from "@mikro-orm/core";
import { Field, ObjectType, Int } from "type-graphql";
import { Author } from "./Author";

@ObjectType()
@Entity()
export class Post {
  @Field(() => Int)
  @PrimaryKey()
  id!: number;

  @Field(() => Author)
  @ManyToOne()
  author!: Author;

  @Field(() => String)
  @Property({ columnType: 'text' })
  message!: string; //TODO Perhaps better as a collection of (string | linebreak | mention) items. Linebreaks and mentions currently require string parsing; not ideal.

  @Field(() => String)
  @Property({ columnType: 'timestamp' })
  createdAt = new Date();

  @Field(() => String)
  @Property({ columnType: 'timestamp', onUpdate: () => new Date() })
  updatedAt = new Date();

  @Field(() => Boolean)
  @Property({ onCreate: () => false, onUpdate: () => true })
  wasUpdated!: boolean; //TODO Better as an enum: original, edited or deleted.
}