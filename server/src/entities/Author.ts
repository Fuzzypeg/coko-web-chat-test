import { Entity, PrimaryKey, Property } from "@mikro-orm/core";
import { Field, ObjectType, Int } from "type-graphql";

@ObjectType()
@Entity()
export class Author {
  @Field(() => Int)
  @PrimaryKey()
  id!: number;

  //TODO Why do I often (but not always) get "Cannot return null for non-nullable field Author.name."? E.g. with query:
  //{ posts { id, author { name } } }
  @Field(() => String, {nullable: false})
  @Property({ columnType: 'text', nullable : false })
  name!: string;

  @Field(() => Boolean)
  @Property({ onCreate: () => true })
  isActive!: boolean;
}