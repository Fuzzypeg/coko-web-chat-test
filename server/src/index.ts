import "reflect-metadata";
import { MikroORM } from "@mikro-orm/core";
import { __prod__ } from "./constants";
import mikroConfig from "./mikro-orm.config";
import express from 'express';
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { PostResolver } from "./resolvers/post";
import { AuthorResolver } from "./resolvers/author";
// import { Author } from "./entities/Author";
// import { Post } from "./entities/Post";

const PORT = 4000;

const main = async () => {
  const orm = await MikroORM.init(mikroConfig);
  await orm.getMigrator().up();
  // const authorA = orm.em.create(Author, {name: "Abelard"});
  // const authorB = orm.em.create(Author, {name: "Balthazar"});
  // const postA = orm.em.create(Post, {author: 1, message: "Χαῖρε!"});
  // const postB = orm.em.create(Post, {author: 2, message: "Ἀσπάζομαι!"});
  // await orm.em.persistAndFlush([authorA, authorB, postA, postB]);

  const app = express();
  
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [PostResolver, AuthorResolver], //This example is greatly simplified to assume only a single chat in a single team.
      validate: false,
    }),
    context: () => ({ em: orm.em })
  });
  apolloServer.applyMiddleware({ app });
  app.listen(PORT, () => {
  console.log(`Server ready at http://localhost:${PORT}${apolloServer.graphqlPath}`)
  console.log(`Subscriptions ready at ws://localhost:${PORT}${apolloServer.subscriptionsPath}`)
  });
}

main();