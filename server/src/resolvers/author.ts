import { Author } from "../entities/Author";
import { MyContext } from "../types";
import { Args, ArgsType, Ctx, Field, Int, Query, Resolver } from "type-graphql";

@ArgsType()
class AuthorArgs {
  @Field(type => Int, { nullable: false })
  id: number;
}

  
@Resolver()
export class AuthorResolver {
  @Query(() => [Author])
  authors(@Ctx() {em}: MyContext): Promise<Author[]> {
    return em.find(Author, {});
  }

  @Query(() => Author)
  author(@Args() {id}: AuthorArgs
    @Ctx() {em}: MyContext): Promise<Author> {
    return em.findOneOrFail(Author, {id: id});
  }
}