import { Post } from "../entities/Post";
import { MyContext } from "../types";
import { Arg, Args, ArgsType, Ctx, Field, Int, Mutation, PubSub, PubSubEngine, Query, Resolver, Root, Subscription } from "type-graphql";
import { QueryOrder } from "@mikro-orm/core";

@ArgsType()
class AddPostArgs {
  @Field(type => Int, { nullable: false })
  authorId: number;

  @Field({ nullable: false })
  message: string;

  @Field(type => [Int], { nullable: true })
  mentionedAuthorIds: number[];
}

@ArgsType()
class PostRangeArgs {
    @Field(type => Int, { nullable: false })
    minId: number;

    @Field(type => Int, { nullable: false })
    maxId: number;
}

@Resolver()
export class PostResolver {
  @Query(() => [Post])
  posts(@Ctx() {em}: MyContext): Promise<Post[]> {
    return em.find(Post, {}, { orderBy: { id: QueryOrder.ASC }}); //Ordering by ID should be fine. Race conditions may occasionally result in dates appearing misordered by one second.
  }

  @Query(() => [Post])
  postRange(@Args() {minId, maxId}: PostRangeArgs,
    @Ctx() {em}: MyContext
  ): Promise<Post[]> {
    return em.find(Post, {id: {$gte: minId, $lt: maxId}}, { orderBy: { id: QueryOrder.ASC }});
  }

  @Query(() => Int!)
  postCount(@Ctx() {em}: MyContext): Promise<number> {
    return em.count(Post);
  }

  //TODO This should obtain authorId from session info rather than allow the client to nominate authorId
  @Mutation(() => Post, {})
  async addPost(@Args() {authorId, message, mentionedAuthorIds}: AddPostArgs,
    @PubSub() pubSub: PubSubEngine,
    @Ctx() {em}: MyContext
  ): Promise<Post> {
    const post = em.create(Post, {author: authorId, message: message});
    await em.persistAndFlush(post);
    await pubSub.publish("POSTS", post);
    if (mentionedAuthorIds)
      console.log("Mentioned authors " + mentionedAuthorIds.join(", "));

    console.log("addPost: " + JSON.stringify(post));//TODO remove
    return post;
  }

  @Subscription({ topics: "POSTS" })
  postAdded(@Root() post: Post): Post {
    console.log("postAdded: " + JSON.stringify(post));//TODO remove
    return post;
  }

  
  
}