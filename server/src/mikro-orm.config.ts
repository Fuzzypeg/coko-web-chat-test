import { __prod__ } from "./constants";
import { Post } from "./entities/Post";
import { MikroORM } from "@mikro-orm/core"
import path from "path";
import { Author } from "./entities/Author";

export default {
  migrations: {
    path: path.join(__dirname, "./migrations"),
    pattern: /^[\w-]+\d+\.[tj]s$/
  },
  dbName: 'cokoslack',
  type: 'postgresql',
  debug: !__prod__,
  user: 'postgres',
  password: 'sergtsop',
  entities: [Post, Author],
  forceUtcTimezone: true
} as Parameters<typeof MikroORM.init>[0];


