import { Migration } from '@mikro-orm/migrations';

export class Migration20201227074252 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "author" ("id" serial primary key, "name" text not null, "is_active" bool not null);');

    this.addSql('create table "post" ("id" serial primary key, "author_id" int4 not null, "message" text not null, "created_at" timestamp not null, "updated_at" timestamp not null, "was_updated" bool not null);');

    this.addSql('alter table "post" add constraint "post_author_id_foreign" foreign key ("author_id") references "author" ("id") on update cascade;');
  }

}
